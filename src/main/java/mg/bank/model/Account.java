package mg.bank.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@Entity
@Table(name = "accounts")
@JacksonXmlRootElement(localName = "Account")
public class Account implements Serializable{
    @Id
    @GeneratedValue
    @JacksonXmlProperty(isAttribute = true)
    private long id;

    @NotNull
    @JacksonXmlProperty
    private long accountNumber;

    @NotNull
    @JacksonXmlProperty
    private String userName;

    @NotNull
    @JacksonXmlProperty
    private String password;

    @NotNull
    @JacksonXmlProperty
    private long balance;

    @JacksonXmlProperty
    @Value("CLIENT")
    private String role;

    @NotNull
    @JacksonXmlProperty
    private String name;

    @NotNull
    @JacksonXmlProperty
    private String firstName;

    @NotNull
    @JacksonXmlProperty
    private String address;

    @NotNull
    @JacksonXmlProperty
    private String profession;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JacksonXmlProperty
    private Date createdAt;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JacksonXmlProperty
    private Date updatedAt;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JacksonXmlProperty
    private Date deletedAt;

    @OneToMany(mappedBy = "myAccount")
    @Transient
    private List<Operation> operations;

    public Account() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String identity) {
        this.userName = identity;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
