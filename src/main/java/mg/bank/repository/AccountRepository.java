package mg.bank.repository;

import org.springframework.data.repository.CrudRepository;

import mg.bank.model.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {
}
