package mg.bank.repository;

import org.springframework.data.repository.CrudRepository;

import mg.bank.model.Operation;

public interface OperationRepository extends CrudRepository<Operation, Long> {
}
