package mg.bank.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import mg.bank.model.Account;
import mg.bank.repository.AccountRepository;

@CrossOrigin(origins = "*")
@Controller
public class MainController {
    @Autowired
    private AccountRepository accountRepository;

//    @Autowired
//    BCryptPasswordEncoder bcrypt;
    BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();


    @GetMapping(value = "/login")
    public ModelAndView login() {
        ModelAndView model = new ModelAndView("loginPage");
        return model;
    }

    @GetMapping(value = { "/", "/home" })
    public ModelAndView home() {
        ModelAndView model = new ModelAndView("homePage");
        return model;
    }

    @GetMapping(value = "/account.create")
    public ModelAndView createAccount() {
        ModelAndView model = new ModelAndView("inscription");
        model.addObject("account", new Account());
        return model;
    }

    @GetMapping(value = "/history")
    public ModelAndView operations() {
        ModelAndView model = new ModelAndView("history");
        return model;
    }

    @GetMapping(value = "/operation")
    public ModelAndView debiter() {
        ModelAndView model = new ModelAndView("operation");
        return model;
    }

    @GetMapping(value = "/profil")
    public ModelAndView profil() {
        ModelAndView model = new ModelAndView("profil");
        return model;
    }

    @PostMapping(value = "/account.store")
    public String store(@ModelAttribute @Valid Account account, BindingResult result) {
        if (result.hasErrors()) {
            System.err.println(result);
            return "redirect:/account.create";
        }
        Date d = new Date();
        account.setCreatedAt(d);
        account.setRole("CLIENT");
        account.setAccountNumber(d.getTime());
        account.setPassword(bcrypt.encode(account.getPassword()));
        accountRepository.save(account);
        return "redirect:/login";
    }
}
